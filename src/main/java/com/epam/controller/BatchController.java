package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BatchDto;
import com.epam.service.BatchServiceImpl;

import jakarta.validation.Valid;

@RestController
@RequestMapping("batches")
public class BatchController {
	@Autowired
	BatchServiceImpl batchService;
	
	@GetMapping
	public ResponseEntity<List<BatchDto>> getAllbatches() {	
		return new ResponseEntity<>(batchService.getAllBatches(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<BatchDto> getBatchById(@PathVariable int id){
		return new ResponseEntity<>(batchService.getBatchById(id), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value= HttpStatus.NO_CONTENT)
	public void deleteBatch(@PathVariable int id) {  
		batchService.deleteBatch(id);	
	}
	
	@PostMapping
	public ResponseEntity<BatchDto> addBook(@Valid @RequestBody BatchDto batchDto) {
		return new ResponseEntity<>(batchService.createBatch(batchDto), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<BatchDto> modifyBatch(@PathVariable int id,@Valid @RequestBody BatchDto batchDto){
		return new ResponseEntity<>(batchService.modifyBatch(id,batchDto), HttpStatus.OK);
	}

}
