package com.epam.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class ExceptionType {
	String timestamp;
	String status;
	String error;
	String path;	
}
