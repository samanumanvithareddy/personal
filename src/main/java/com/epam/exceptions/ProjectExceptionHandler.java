package com.epam.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ProjectExceptionHandler {
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionType handleMethodArgumentNotValidException(MethodArgumentNotValidException e,WebRequest request) {
		List<String> error = new ArrayList<>();
		e.getAllErrors().forEach(err -> error.add(err.getDefaultMessage()));
		log.error("MethodArgumentNotValidException :{}",error);
		return new ExceptionType(new Date().toString(),
				HttpStatus.BAD_REQUEST.toString(),
				error.toString(),
				request.getDescription(false));
	}		
		
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionType handleMethodArgumentMismatchException(MethodArgumentTypeMismatchException e,WebRequest request) {
		log.error("MethodArgumentTypeMismatchException :{}",ExceptionUtils.getStackTrace(e));
		return new ExceptionType(new Date().toString(),
				HttpStatus.BAD_REQUEST.toString(),
				e.getMessage(),
				request.getDescription(false));
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionType handleHttpMessageNotReadableException(HttpMessageNotReadableException e,WebRequest request) {
		log.error("HttpMessageNotReadableException :{}","Invalid inputs ");
		return new ExceptionType(new Date().toString(),
				HttpStatus.BAD_REQUEST.toString(),
				e.getMessage(),
				request.getDescription(false));
	}
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionType handleRuntimeException(RuntimeException e,WebRequest request) {
		log.error("Encountered RuntimeException :{}",ExceptionUtils.getStackTrace(e)); //
		return new ExceptionType(new Date().toString(),
				HttpStatus.INTERNAL_SERVER_ERROR.toString(),
				e.getMessage(),  
				request.getDescription(false));
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ExceptionType handleDataIntegrityException(DataIntegrityViolationException e, WebRequest request) {
		return new ExceptionType(new Date().toString(), 
				HttpStatus.CONFLICT.toString(),
				"Branch with name already exists",
				request.getDescription(true));
	}
	@ExceptionHandler(BatchException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionType handleBatchException(BatchException e,WebRequest request) {
		log.error("BatchException :{}",ExceptionUtils.getStackTrace(e));
		return new ExceptionType(new Date().toString(),
				HttpStatus.NOT_FOUND.toString(),
				e.getMessage(),
				request.getDescription(false));
	}
	
}
