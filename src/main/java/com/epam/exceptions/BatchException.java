package com.epam.exceptions;

public class BatchException extends RuntimeException{
	public BatchException(String message) {
		super(message);
	}

}
