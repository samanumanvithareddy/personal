package com.epam.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.BatchDto;
import com.epam.entity.Batch;
import com.epam.exceptions.BatchException;
import com.epam.repository.BatchRepository;

import jakarta.transaction.Transactional;


@Service
public class BatchServiceImpl implements BatchService{
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired	
	ModelMapper modelMapper;

	@Override
	public BatchDto createBatch(BatchDto batchDto) {
		Batch batch = modelMapper.map(batchDto,Batch.class);
		return modelMapper.map(batchRepository.save(batch),BatchDto.class);	
	}

	@Override
	public void deleteBatch(int batchNo) {
		batchRepository.deleteById(batchNo);
		
	}

	@Override
	public List<BatchDto> getAllBatches() {
		return batchRepository.findAll().stream().map(batch->modelMapper.map(batch, BatchDto.class)).toList();
	}

	@Override
	public BatchDto getBatchById(int id) {
		return batchRepository.findById(id).map(batch -> {
			return modelMapper.map(batch, BatchDto.class);
		})
				.orElseThrow(()-> new BatchException("Batch with id "+id+ " does not exist"));
	}

	@Override
	@Transactional
	public BatchDto modifyBatch(int id, BatchDto batchDto) {
		if(batchRepository.existsById(id)) {
			batchDto.setId(id);
			Batch batch = modelMapper.map(batchDto,Batch.class);
			return modelMapper.map(batchRepository.save(batch),BatchDto.class);	
		}
		else {
			throw new BatchException("Batch does not exist");
		}
	}

}
