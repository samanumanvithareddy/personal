package com.epam.service;

import java.util.List;

import com.epam.dto.BatchDto;

public interface BatchService {
	BatchDto createBatch(BatchDto batchDto);
	void deleteBatch(int batchNo);
	List<BatchDto> getAllBatches();
	BatchDto getBatchById(int id);
	BatchDto modifyBatch(int id, BatchDto batchDto);

}
