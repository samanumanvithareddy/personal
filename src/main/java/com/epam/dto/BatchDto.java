package com.epam.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
	
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class BatchDto {
	
	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	
	@NotBlank(message="Name should not be empty. Provide an appropriate name")
	private String name;
	
	@Size(min=1, message="There should atleast be 1 student")
	@Valid
	List<StudentDto> students=new ArrayList<>();

	
	
	
}
