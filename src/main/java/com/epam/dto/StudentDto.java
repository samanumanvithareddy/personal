package com.epam.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class StudentDto {
	
	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	
	@NotBlank(message="Name should not be empty. Give an appropriate name")
	private String name;
	
	
	
	

}
