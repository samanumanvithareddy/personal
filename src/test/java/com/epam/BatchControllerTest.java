package com.epam;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.BatchController;
import com.epam.dto.BatchDto;
import com.epam.dto.StudentDto;
import com.epam.service.BatchServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(BatchController.class)
class BatchControllerTest {
	@MockBean
	BatchServiceImpl batchService;
	@Autowired
	private MockMvc mockMvc;
	
	BatchDto batchDto;
	BatchDto dummyBatch;
	StudentDto studentDto;
		
	List<StudentDto> studentDtoList=new ArrayList<>();
	List<BatchDto> batchDtoList=new ArrayList<>();
			
	@BeforeEach
	public void setUp() {	
		studentDto=new StudentDto();
		studentDto.setId(1);
		studentDto.setName("manvitha");
		studentDtoList.add(studentDto);
		
		batchDto=new BatchDto();
		batchDto.setId(1);
		batchDto.setName("java");
		batchDto.setStudents(studentDtoList);
		batchDtoList.add(batchDto);
		
		dummyBatch=new BatchDto();
	}
	
	@Test
	void testCreateBatch() throws Exception{
		Mockito.when(batchService.createBatch(any())).thenReturn(batchDto);
		mockMvc.perform(post("/batches")
		.contentType(MediaType.APPLICATION_JSON)
		.content(new ObjectMapper().writeValueAsString(batchDto)))
		.andExpect(status().isCreated())
		.andReturn();
	}

	@Test
	void testViewAllBatches() throws Exception {
	    Mockito.when(batchService.getAllBatches()).thenReturn(batchDtoList);
	    mockMvc.perform(get("/batches"))
	           .andExpect(status().isOk())
	           .andReturn();
	}
	
	@Test
	void testGetBatchById() throws Exception{
		Mockito.when(batchService.getBatchById(1)).thenReturn(batchDto);
		mockMvc.perform(get("/batches/{id}",1)).andExpect(status().isOk())
	            .andReturn();
	}
	@Test
	void testDeleteBatch() throws Exception{
		Mockito.doNothing().when(batchService).deleteBatch(1);
		mockMvc.perform(delete("/batches/{id}",1)).andExpect(status().isNoContent())
				.andReturn();
	}
	@Test
	void testModifyBatch() throws Exception{
		Mockito.when(batchService.modifyBatch(1,batchDto)).thenReturn(batchDto);
		mockMvc.perform(put("/batches/{id}",1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(batchDto)))
                .andExpect(status().isOk())
                .andReturn();
	}
	@Test
    void testMethodNotValid() throws JsonProcessingException, Exception {
    	Mockito.when(batchService.createBatch(dummyBatch)).thenReturn(dummyBatch);
    	mockMvc.perform(post("/batches").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(dummyBatch)))
    	.andExpect(status().isBadRequest())
    	.andReturn();
    	
    }
    @Test
    void testMethodArgMismatch() throws Exception {
    	Mockito.when(batchService.getBatchById(1)).thenReturn(batchDto);
    	mockMvc.perform(get("/batches/Sum")).andExpect(status().isBadRequest()).andReturn();
    }
    
//    @Test 
//    void testDataIntegrityViolation() throws Exception{
//    	Mockito.when(batchService.createBatch(any())).thenReturn(batchDto);
//		mockMvc.perform(post("/batches")
//		.contentType(MediaType.APPLICATION_JSON)
//		.content(new ObjectMapper().writeValueAsString(batchDto)))
//		.andExpect(status().isCreated())
//		.andReturn();
//    }
    
    @Test
    void testhttpNotReadble() throws JsonProcessingException, Exception {
    	Mockito.when(batchService.createBatch(dummyBatch)).thenReturn(dummyBatch);
    	mockMvc.perform(post("/batches").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString("{name:")))
    	.andExpect(status().isBadRequest())
    	.andReturn();
    }
}
