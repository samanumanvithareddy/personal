package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.BatchDto;
import com.epam.dto.StudentDto;
import com.epam.entity.Batch;
import com.epam.entity.Student;
import com.epam.exceptions.BatchException;
import com.epam.repository.BatchRepository;
import com.epam.service.BatchServiceImpl;

@ExtendWith(MockitoExtension.class)
class BatchServiceImplTest {
	@Mock
	BatchRepository batchRepository;
	
	@Mock
	ModelMapper modelMapper;
	
	@InjectMocks
	BatchServiceImpl batchService;
		
	Batch batch=new Batch();
	BatchDto batchDto;	
	StudentDto studentDto;
	Student student;	
	List<StudentDto> studentDtoList=new ArrayList<>();
	List<Student> studentList=new ArrayList<>();
	
	List<BatchDto> batchDtoList=new ArrayList<>();
	List<Batch> batchList=new ArrayList<>();
	
	@BeforeEach
	public void setUp() {	
		student=new Student();
		student.setId(1);
		student.setName("manvitha");
		studentList.add(student);
		
		studentDto=new StudentDto();
		studentDto.setId(1);
		studentDto.setName("manvitha");
		studentDtoList.add(studentDto);
		
		batchDto=new BatchDto();
		batchDto.setId(1);
		batchDto.setName("java");
		batchDto.setStudents(studentDtoList);
		batchDtoList.add(batchDto);
		
		batch=new Batch();
		batch.setId(1);
		batch.setName("java");
		batch.setStudents(studentList);
		batchList.add(batch);
	}
	
	@Test
	void testCreateBatch() {
		Mockito.when(modelMapper.map(batchDto,Batch.class)).thenReturn(batch);
		Mockito.when(batchRepository.save(any())).thenReturn(batch);
		Mockito.when(modelMapper.map(batch,BatchDto.class)).thenReturn(batchDto);
		BatchDto saved=batchService.createBatch(batchDto);
		assertEquals(saved, batchDto);
		
		Mockito.verify(modelMapper).map(batchDto, Batch.class);
		Mockito.verify(batchRepository).save(any());
		Mockito.verify(modelMapper).map(batch, BatchDto.class);
	}
	
	@Test
	void testGetAllBatches() {
		Mockito.when(batchRepository.findAll()).thenReturn(List.of(batch));
		Mockito.when(modelMapper.map(batch,BatchDto.class)).thenReturn(batchDto);
		List<BatchDto> batchesList=batchService.getAllBatches();
		assertNotNull(batchesList);
		
		Mockito.verify(batchRepository).findAll();
	}
	
	@Test 
	void testGetBatchById(){
		Mockito.when(batchRepository.findById(1)).thenReturn(Optional.of(batch));
		Mockito.when(modelMapper.map(batch,BatchDto.class)).thenReturn(batchDto);
		BatchDto retrievedBatch=batchService.getBatchById(1);
		assertEquals(retrievedBatch, batchDto);
		
		Mockito.verify(batchRepository).findById(1);
		Mockito.verify(modelMapper).map(batch,BatchDto.class);
	}
	
	@Test 
	void testGetBatchByIdException(){
		Mockito.when(batchRepository.findById(2000)).thenReturn(Optional.empty());
		assertThrows(BatchException.class,()->batchService.getBatchById(2000));
	}
	
	@Test
	void testDeleteBatch() {
		Mockito.doNothing().when(batchRepository).deleteById(1);
		batchService.deleteBatch(1);
		Mockito.verify(batchRepository).deleteById(1);
	}
	
	@Test
	void testModifyBatch() { 
		Mockito.when(batchRepository.existsById(1)).thenReturn(true);
		Mockito.when(modelMapper.map(batchDto,Batch.class)).thenReturn(batch);
		Mockito.when(batchRepository.save(any())).thenReturn(batch);
		Mockito.when(modelMapper.map(batch,BatchDto.class)).thenReturn(batchDto);
		BatchDto modifiedBatch=batchService.modifyBatch(1,batchDto);
		
		assertEquals(modifiedBatch, batchDto);
		assertNotNull(batch.getId());
		assertNotNull(batch.getName());
		assertNotNull(batch.getStudents());
		
		assertNotNull(student.getBatch());
		assertNotNull(student.getId());
		assertNotNull(student.getName());
		
		Mockito.verify(batchRepository).existsById(1);
		Mockito.verify(modelMapper).map(batchDto,Batch.class);
		Mockito.verify(batchRepository).save(any());
	}
	
	@Test 
	void testModifyBatchException() {
		Mockito.when(batchRepository.existsById(100)).thenReturn(false);
		assertThrows(BatchException.class,()->batchService.modifyBatch(100,batchDto));
	}
	
	
}
